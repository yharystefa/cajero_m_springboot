// JavaScript source code
angular.module('cajero',[])
.controller('filasCuentas',function($scope,$http){
    
    $scope.nombre = "";
    $scope.edad = "";
    $scope.correo = "";
    
    // accion del boton consultar
    $scope.consultar = function(){
        if($scope.cedula == undefined || $scope.cedula == null){
            $scope.cedula = 0;
        }
        
        $http.get("/estudiante?cedula="+$scope.cedula).then(function(data){
            console.log(data.data);
            $scope.nombre = data.data.nombre;
            $scope.edad = data.data.edad;
            $scope.correo = data.data.correo;
        },function(){
             //error
            $scope.nombre = "";
            $scope.edad = "";
            $scope.correo = "";
            $scope.filas = []; // guarda
        });
        
        $http.get("/cuentas?cedula="+$scope.cedula).then(function(data){
            console.log(data.data);
            $scope.filas = data.data;  
        },function(){
            $scope.filas = [];
        });
    };
    
    //acción del botón actualizar
    $scope.actualizar = function(){
        if($scope.cedula == undefined || $scope.cedula == null){
            $scope.cedula = 0;
        }
        data = {
            "id": $scope.cedula,
            "nombre":$scope.nombre,
            "edad": $scope.edad,
            "correo": $scope.correo
        }
        $http.post('/estudiante', data).then(function(data){
            //success
            $scope.nombre = data.data.nombre;
            $scope.edad = data.data.edad;
            $scope.correo = data.data.correo;
        },function(){
            //error
            $scope.nombre = "";
            $scope.edad = "";
            $scope.correo = "";
            $scope.filas = [];
        });
        
    };
    // acción del botón borrar
    $scope.borrar = function(){
        if($scope.idcuenta == undefined || $scope.idcuenta == null){
            $scope.idcuenta = 0;
        }
        data = {
            "id": $scope.idcuenta
        }
        $http.delete('/eliminarcuenta/'+$scope.idcuenta, data).then(function(data){
            alert("La cuenta ha sido eliminada");
            
        },function(){
            alert("Ha ocurrido un error");
        });
    };
});
    
