/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

// CLASE PADRE
package com.mintic.cajero.logica;

import java.sql.SQLException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;

/**
 *
 * @author frey
 */
public abstract class Transaccion {
    
    
    @Autowired
    JdbcTemplate jdbcTemplate;
    // Atributos
    private int id;
    private String tipo;
    private float valor;
    
    // Métodos Getters y Setters
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getTipo() {
        return tipo;
    }

    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    public float getValor() {
        return valor;
    }

    public void setValor(float valor) {
        this.valor = valor;
    }
    
    // Método
    public void registrarTransaccion() throws ClassNotFoundException, SQLException{
        String  sql = "INSERT INTO transacion(tipo, valor) VALUES(?,?);";
        jdbcTemplate.execute(sql);
    }
    
    @Override
    public String toString() {
        return "Transaccion{" + "id=" + id + ", tipo=" + tipo + ", valor=" + valor + '}';
    }
}
